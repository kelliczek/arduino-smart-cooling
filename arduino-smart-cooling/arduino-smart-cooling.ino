#include "DHT.h"
// install DHT Sensor library (v1.4.1+)

#define DHT_PIN 2        // digital pin connected to the DHT sensor
#define DHT_TYPE DHT11   // DHT 11
#define GATE_PIN 10      // digital pwm pin connected to the mosfet's gate
#define LED_POWER_PIN 7  // digital pin for power LED
#define LED_FAN_PIN 8    // digital ping for fan LED

bool DEBUG_MODE = false;  // set TRUE for serial output
float humidity = 0.0;
float temperature = 0.0;
float humidity_threshold = 20.0;     // change threshold here
float temperature_threshold = 28.0;  // change threshold here

DHT dht(DHT_PIN, DHT_TYPE);  // init DHT sensor

void setup() {
  if (DEBUG_MODE == true) {
    Serial.begin(9600);
    Serial.println("Serial Communication Initialized");
    Serial.println("Unit Starting Up");
  }

  dht.begin();  // starting to measure temperature and humidity

  pinMode(GATE_PIN, OUTPUT);       // setting pwm pin of the gate to output
  digitalWrite(GATE_PIN, LOW);     // turning off gate (fan off)
  pinMode(LED_POWER_PIN, OUTPUT);  // setting power LED pin to output
  pinMode(LED_FAN_PIN, OUTPUT);    // setting fan LED pint to output
  digitalWrite(LED_FAN_PIN, LOW);  // turning off fan LED

  // init - power LED starting up
  for (int i = 0; i < 5; i++) {
    digitalWrite(LED_POWER_PIN, HIGH);
    delay(500);
    digitalWrite(LED_POWER_PIN, LOW);
    delay(500);
  }
}

void loop() {
  digitalWrite(LED_POWER_PIN, LOW);
  delay(30000);  // this is for DHT to start measuring

  humidity = dht.readHumidity();
  temperature = dht.readTemperature();

  // check for failed readings (and try again)
  if (isnan(humidity) || isnan(temperature)) {
    if (DEBUG_MODE == true) {
      Serial.println("Failed to read from DHT sensor!");
    }
    return;
  }

  // show sensor readings if DEBUG_MODE is true
  if (DEBUG_MODE == true) {
    Serial.print("Humidity: " + String(humidity) + "% (threshold: " + String(humidity_threshold) + "%)   Temperature: " + String(temperature) + "°C (threshold: " + String(temperature_threshold) + "°C)\n\n");
  }

  // check for excessive humidity or temperature
  if ((humidity <= humidity_threshold) || (temperature >= temperature_threshold)) {
    digitalWrite(LED_FAN_PIN, HIGH);
    digitalWrite(GATE_PIN, HIGH);
    delay(30000);  // turn on fan for straight 30s
  } else {
    digitalWrite(LED_FAN_PIN, LOW);
    digitalWrite(GATE_PIN, LOW);  // turn off fan
  }

  digitalWrite(LED_POWER_PIN, HIGH);
  delay(1000);
}
