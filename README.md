# Arduino Smart Cooling

Small project using Arduino board. DHT11 sensor controls fan operation.

## Prerequisites

Install DHT Sensor library (v1.4.1+)


## Code setup

To change humidity threshold (default 20%)
`float humidity_threshold = 20.0;    // change threshold here`

To change temperature threshold (default 28°C)
`float temperature_threshold = 28.0; // change threshold here`



## Component list

* 5V PSU
* Arduino board
* Charging cable
* DHT11 Humidity and Temperature sensor
* IRF530N Mosfet
* 5V DC 12V Boost Module
* 12V Fan
* Red LED
* Yellow LED
* 2x 200 Ohm Resisor
* Push button
* Wires



## Diagram

 ![picture alt](diagram/diagram.png "Diagram")